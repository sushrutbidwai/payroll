/**
 * 
 */
package myob.domain;

/**
 * Represents a tax bracet for a year.
 * 
 * @author IamSB
 *
 */
public class TaxBracet {

	/** Financial year */
	protected Integer year;
	
	/** Lower salary limit for this bracet */
	protected Integer lowerSalaryLimit;
	
	/** Upper salary limit for this bracet, can be null */
	protected Integer upperSalaryLimit;
	
	/** Base income tax */
	protected Integer baseIncomeTax;
	
	/** Income tax percentage */
	protected Float incomeTaxPercent;

	/**
	 * @param year
	 * @param lowerSalaryLimit
	 * @param upperSalaryLimit
	 * @param baseIncomeTax
	 * @param incomeTaxPercent
	 */
	public TaxBracet(Integer year, Integer lowerSalaryLimit, Integer upperSalaryLimit, Integer baseIncomeTax, Float incomeTaxPercent) {
		super();
		this.year = year;
		this.lowerSalaryLimit = lowerSalaryLimit;
		this.upperSalaryLimit = upperSalaryLimit;
		this.baseIncomeTax = baseIncomeTax;
		this.incomeTaxPercent = incomeTaxPercent;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getLowerSalaryLimit() {
		return lowerSalaryLimit;
	}

	public void setLowerSalaryLimit(Integer lowerSalaryLimit) {
		this.lowerSalaryLimit = lowerSalaryLimit;
	}

	public Integer getUpperSalaryLimit() {
		return upperSalaryLimit;
	}

	public void setUpperSalaryLimit(Integer upperSalaryLimit) {
		this.upperSalaryLimit = upperSalaryLimit;
	}

	public Integer getBaseIncomeTax() {
		return baseIncomeTax;
	}

	public void setBaseIncomeTax(Integer baseIncomeTax) {
		this.baseIncomeTax = baseIncomeTax;
	}

	public Float getIncomeTaxPercent() {
		return incomeTaxPercent;
	}

	public void setIncomeTaxPercent(Float incomeTaxPercent) {
		this.incomeTaxPercent = incomeTaxPercent;
	}

}
