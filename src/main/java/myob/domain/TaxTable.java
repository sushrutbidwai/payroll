/**
 * 
 */
package myob.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Metadata which stores tax table information.<br>
 * Ideally tax bracets should be loaded from config tables or config files.<br>
 * Hence implemented as Singleton.
 * 
 * @author IamSB
 *
 */
public class TaxTable {

	private static TaxTable _instance;

	private Map<Integer, List<TaxBracet>> taxBracetsByYear = new HashMap<Integer, List<TaxBracet>>();

	private TaxTable() {
		loadTaxTableConfiguration();
	}

	public static synchronized TaxTable getInstance() {
		if (_instance == null) {
			_instance = new TaxTable();
		}
		return _instance;
	}

	public List<TaxBracet> getTaxBracetsForYear(Integer year) {
		return taxBracetsByYear.get(year);
	}

	/**
	 * Loads tax table configuration data from config files.<br>
	 * Hard coded for now.
	 */
	private void loadTaxTableConfiguration() {
		//TOD Load tax table from taxtable.json file

		List<TaxBracet> taxBracetsFor2012 = new ArrayList<TaxBracet>();

		TaxBracet taxBracet1 = new TaxBracet(2012, 0, 18200, 0, 0f);
		taxBracetsFor2012.add(taxBracet1);
		TaxBracet taxBracet2 = new TaxBracet(2012, 18200, 37000, 0, 19f);
		taxBracetsFor2012.add(taxBracet2);
		TaxBracet taxBracet3 = new TaxBracet(2012, 37000, 80000, 3572, 32.5f);
		taxBracetsFor2012.add(taxBracet3);
		TaxBracet taxBracet4 = new TaxBracet(2012, 80000, 180000, 17547, 37f);
		taxBracetsFor2012.add(taxBracet4);
		TaxBracet taxBracet5 = new TaxBracet(2012, 180000, null, 54547, 45f);
		taxBracetsFor2012.add(taxBracet5);

		taxBracetsByYear.put(2012, taxBracetsFor2012);
	}

}
