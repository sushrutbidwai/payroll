/**
 * 
 */
package myob.domain;

import java.util.Calendar;

/**
 * Domain object representing parsed input record.
 * 
 * @author IamSB
 *
 */
public class PayslipInputObject {

	private String firstName;
	
	private String lastName;
	
	private Integer annualSalary;
	
	private Float superRate;
	
	private String payPeriod;
	
	private Calendar startDatePeriod;
	
	private Calendar endDatePeriod;

	public Calendar getStartDatePeriod() {
		return startDatePeriod;
	}

	public void setStartDatePeriod(Calendar startDatePeriod) {
		this.startDatePeriod = startDatePeriod;
	}

	public Calendar getEndDatePeriod() {
		return endDatePeriod;
	}

	public void setEndDatePeriod(Calendar endDatePeriod) {
		this.endDatePeriod = endDatePeriod;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAnnualSalary() {
		return annualSalary;
	}

	public void setAnnualSalary(Integer annualSalary) {
		this.annualSalary = annualSalary;
	}

	public Float getSuperRate() {
		return superRate;
	}

	public void setSuperRate(Float superRate) {
		this.superRate = superRate;
	}

	public String getPayPeriod() {
		return payPeriod;
	}

	public void setPayPeriod(String payPeriod) {
		this.payPeriod = payPeriod;
	}

}
