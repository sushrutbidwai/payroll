/**
 * 
 */
package myob.domain;

/**
 * Domain object for Payslip.
 *  
 * @author IamSB
 *
 */
public class Payslip {

	private String firstName;
	
	private String lastName;
	
	private Integer grossIncome;
	
	private Integer superPayment;
	
	private Integer incomeTax;
	
	private Integer netIncome;
	
	private String payPeriod;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getGrossIncome() {
		return grossIncome;
	}

	public void setGrossIncome(Integer grossIncome) {
		this.grossIncome = grossIncome;
	}

	public Integer getSuperPayment() {
		return superPayment;
	}

	public void setSuperPayment(Integer superPayment) {
		this.superPayment = superPayment;
	}

	public Integer getIncomeTax() {
		return incomeTax;
	}

	public void setIncomeTax(Integer incomeTax) {
		this.incomeTax = incomeTax;
	}

	public Integer getNetIncome() {
		return netIncome;
	}

	public void setNetIncome(Integer netIncome) {
		this.netIncome = netIncome;
	}

	public String getPayPeriod() {
		return payPeriod;
	}

	public void setPayPeriod(String payPeriod) {
		this.payPeriod = payPeriod;
	}

}
