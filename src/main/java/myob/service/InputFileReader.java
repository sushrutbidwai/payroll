/**
 * 
 */
package myob.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import myob.domain.PayslipInputObject;
import myob.exception.ValidationException;

/**
 * Provide Input File Reading service.
 * 
 * @author IamSB
 *
 */
public class InputFileReader {

	// Input CSV format.
	private static final CSVFormat INPUT_CSV_FORMAT = CSVFormat.DEFAULT.withTrim().withDelimiter(',');

	// Input validator service.
	private final PayslipInputValidator payslipInputValidator = new PayslipInputValidator();
	
	// Input record parser service.
	private final InputRecordParser inputRecordParser = new InputRecordParser();

	/**
	 * Parses input file using inputCSVFormat.<br>
	 * 
	 * @param inputFilename - Input filename
	 * @return List of validated PayslipInputObjects
	 * @throws ValidationException - if input file format does not match specifications.
	 */
	public List<PayslipInputObject> parseAndValidate(String inputFilename) throws ValidationException {
		
		Reader inputFileReader = null;
		try {
			inputFileReader = new FileReader(inputFilename);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Input file does not exist." + inputFilename, e);
		}

		// Parse input file using common-csv.
		Iterable<CSVRecord> records;
		try {
			records = INPUT_CSV_FORMAT.parse(inputFileReader);
		} catch (IOException e1) {
			throw new RuntimeException("Error in processing file " + inputFilename, e1);
		}

		try {

			List<PayslipInputObject> inputObjects = converRecordsToInputObjects(records);
			
			return inputObjects;

		} catch (ValidationException e) {
			e.setFileName(inputFilename);
			throw e;
		} finally {
			try {
				inputFileReader.close();
			} catch (IOException e) {
				throw new RuntimeException("Exception in processing file " + inputFilename, e);
			}
		}
	}
	
	private List<PayslipInputObject> converRecordsToInputObjects(Iterable<CSVRecord> records) throws ValidationException {
		List<PayslipInputObject> inputObjects = new ArrayList<PayslipInputObject>();
		int lineNum = 0;
		
		try {
			for (CSVRecord record : records) {
				validateInputLength(record);
				PayslipInputObject payslipInput = inputRecordParser.parse(record);
				payslipInputValidator.validate(payslipInput);
				inputObjects.add(payslipInput);
				lineNum++;
			}
		} catch (ValidationException e) {
			// Add one to line number, so non technical user is not confused.
			throw new ValidationException(e.getMessage(), lineNum + 1, e);
		}
		return inputObjects;
	}

	/**
	 * @param input
	 * @throws ValidationException
	 */
	private void validateInputLength(CSVRecord input) throws ValidationException {
		if (input.size() != 5) {
			throw new ValidationException("Validation failed. Input record should have 5 values.");
		}
	}
}
