/**
 * 
 */
package myob.service;

import java.util.List;

import myob.Configuration;
import myob.domain.Payslip;
import myob.domain.PayslipInputObject;
import myob.domain.TaxBracet;
import myob.domain.TaxTable;
import myob.exception.ValidationException;

/**
 * Calculates all required payslip fields.
 * 
 * @author IamSB
 *
 */
public class PayslipCalculator {

	/**
	 * Creates and calculates all Payslip fields.
	 * 
	 * @param input
	 *            - Validated PayslipInputObject
	 * 
	 * @return - Payslip
	 * 
	 * @throws ValidationException
	 *             - if annual salary does not fit any tax bracets due to wrong
	 *             configuration of taxbracets.
	 */
	public Payslip createPayslip(PayslipInputObject input) throws ValidationException {

		Payslip payslip = new Payslip();
		payslip.setFirstName(input.getFirstName());
		payslip.setLastName(input.getLastName());
		payslip.setPayPeriod(input.getPayPeriod());

		Integer monthlySalary = calculateMonthlySalary(input.getAnnualSalary());

		payslip.setGrossIncome(monthlySalary);
		payslip.setIncomeTax(calculateIncomeTax(input));
		payslip.setNetIncome(payslip.getGrossIncome() - payslip.getIncomeTax());
		payslip.setSuperPayment(calculateSuperPayment(monthlySalary, input.getSuperRate()));

		return payslip;
	}

	/**
	 * @param monthlySalary
	 * @param superRate
	 * @return
	 */
	protected Integer calculateSuperPayment(Integer monthlySalary, Float superRate) {
		return Math.round(monthlySalary * superRate / 100);
	}

	/**
	 * @param annualSalary
	 * @return
	 */
	protected Integer calculateMonthlySalary(Integer annualSalary) {
		float monthlySalary = (float) annualSalary / 12;
		return Math.round(monthlySalary);
	}

	/**
	 * @param input
	 * @return
	 * @throws ValidationException
	 */
	protected Integer calculateIncomeTax(PayslipInputObject input) throws ValidationException {
		Integer annualSalary = input.getAnnualSalary();

		// Using hardcoded year.
		// Year should be provided in input file.
		List<TaxBracet> taxBracets = TaxTable.getInstance().getTaxBracetsForYear(Configuration.FINANCIAL_YEAR);
		for (TaxBracet taxBracet : taxBracets) {
			if (annualSalary > taxBracet.getLowerSalaryLimit() && (taxBracet.getUpperSalaryLimit() == null || annualSalary <= taxBracet.getUpperSalaryLimit())) {
				return calculateIncomeTax(taxBracet, input);
			}
		}
		throw new ValidationException("Annual salary does not fall in any  tax bracet, check configuration data for tax bracets.");
	}

	/**
	 * @param taxBracet
	 * @param input
	 * @return
	 */
	protected Integer calculateIncomeTax(TaxBracet taxBracet, PayslipInputObject input) {
		float incomeTax = (taxBracet.getBaseIncomeTax() + (input.getAnnualSalary() - taxBracet.getLowerSalaryLimit()) * taxBracet.getIncomeTaxPercent() / 100) / 12;
		return Math.round(incomeTax);
	}

}
