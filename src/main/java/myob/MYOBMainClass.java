/**
 * 
 */
package myob;

import myob.api.PayslipGeneratorService;
import myob.exception.ValidationException;

/**
 * Main Class to run from command line.
 * 
 * @author IamSB
 *
 */
public class MYOBMainClass {

	/**
	 * Validate input and output filenames are provided.
	 * 
	 * @param args
	 */
	public Boolean validateArguments(String[] args) {
		if (args.length != 2) {
			throw new IllegalArgumentException("Input and/or output filename missing.");
		}
		return true;
	}

	/**
	 * Generate payslips by calling PayslipGeneratorService api.
	 * 
	 * @param args
	 */
	public void generatePayslip(String args[]) {
		String inputFilename = args[0];
		String outputFilename = args[1];
		System.out.println("Reading input from : " + inputFilename);
		System.out.println("Writing output to : " + outputFilename);

		PayslipGeneratorService payslipGeneratorService = new PayslipGeneratorService();
		try {
			payslipGeneratorService.generatePayslips(inputFilename, outputFilename);
			System.out.println("Output file " + outputFilename + " has been successfully generated.");
		} catch (ValidationException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Main runner.
	 * 
	 * @param args
	 *            - should contains 2 parameters - input file name - output file
	 *            name
	 */
	public static void main(String[] args) {

		MYOBMainClass mainClass = new MYOBMainClass();
		
		if (mainClass.validateArguments(args)) {
			mainClass.generatePayslip(args);
		}
	}

}
