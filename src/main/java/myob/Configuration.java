/**
 * 
 */
package myob;

import java.text.SimpleDateFormat;

/**
 * Configurations.<br>
 * 
 * Can be easily externalised for more extensibility.<br>
 * 
 * @author IamSB
 *
 */
public class Configuration {

	public static final Integer FINANCIAL_YEAR = 2012;

	/**
	 * Input date format: dd MMM yyyy<br>
	 * Examples: 01 March 2012, 01 Mar 2012<br>
	 * 
	 */
	public static final SimpleDateFormat INPUT_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");
}
