/**
 * 
 */
package myob.api;

import java.util.ArrayList;
import java.util.List;

import myob.domain.Payslip;
import myob.domain.PayslipInputObject;
import myob.exception.ValidationException;
import myob.service.InputFileReader;
import myob.service.OutputFilePrinter;
import myob.service.PayslipCalculator;

/**
 * API for generating payslips.
 * 
 * @author IamSB
 *
 */
public class PayslipGeneratorService {

	// Input file reader service.
	private InputFileReader inputFileReader = new InputFileReader();

	// Output file printer service.
	private OutputFilePrinter outputFilePrinter = new OutputFilePrinter();

	// Payslip calculator service.
	private PayslipCalculator payslipCalculator = new PayslipCalculator();

	/**
	 * Parse and validate input file<br>
	 * Generate payslips for each parsed record<br>
	 * Print output file with generated payslips<br>
	 * 
	 * @param inputFilename - Input file name
	 * @param outputFilename - Output file name
	 * @throws ValidationException
	 *             - If validation failed, exception is thrown containing why
	 *             validation failed and at which line validation failed.
	 */
	public void generatePayslips(String inputFilename, String outputFilename) throws ValidationException {
		// Read, parse, validate input file.
		List<PayslipInputObject> inputRecords = inputFileReader.parseAndValidate(inputFilename);

		// For each parse records, generate payslip.
		List<Payslip> payslips = new ArrayList<Payslip>();
		for (PayslipInputObject inputRecord : inputRecords) {
			Payslip payslip = payslipCalculator.createPayslip(inputRecord);
			payslips.add(payslip);
		}

		// Print generated payslips to outputfile.
		outputFilePrinter.printOutputFile(outputFilename, payslips);
	}

}
