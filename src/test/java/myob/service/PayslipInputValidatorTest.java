/**
 * 
 */
package myob.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.fail;

import java.text.ParseException;

import org.junit.Test;

import myob.domain.PayslipInputObject;
import myob.exception.ValidationException;
import myob.helper.Helper;
import myob.utils.Utils;

/**
 * Test : myob.service.PayslipInputValidator
 * 
 * @author IamSB
 *
 */
public class PayslipInputValidatorTest {

	private PayslipInputValidator validator = new PayslipInputValidator();

	/**
	 * Test method for
	 * {@link myob.service.PayslipInputValidator#validate(myob.domain.PayslipInputObject)}
	 * .
	 */
	@Test
	public void testValidateWithValidData() {
		PayslipInputObject inputObject = Helper.populatePayslipObject();
		try {
			Boolean valid = validator.validate(inputObject);
			assertThat(valid).isTrue();
		} catch (ValidationException e) {
			fail("should not throw validation exception");
		}
	}

	/**
	 * Test method for
	 * {@link myob.service.PayslipInputValidator#validate(myob.domain.PayslipInputObject)}
	 * .
	 */
	@Test
	public void testValidateWithEndDateBeforeStartDate() {
		PayslipInputObject inputObject = Helper.populatePayslipObject();
		try {
			inputObject.setStartDatePeriod(Utils.parseInputDate("31 March 2012"));
			inputObject.setEndDatePeriod(Utils.parseInputDate("1 March 2012"));
		} catch (ParseException e1) {
			fail("Should not throw ParseException");
		}
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("End date must be before start date");
	}

	/**
	 * Test: Missing first name.
	 * 
	 * Test method for
	 * {@link myob.service.PayslipInputValidator#validate(myob.domain.PayslipInputObject)}
	 * .
	 */
	@Test
	public void testValidateMissingFirstName() {
		PayslipInputObject inputObject = Helper.populatePayslipObject();
		inputObject.setFirstName("");
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("First name");
	}

	/**
	 * Test: Missing last name.
	 * 
	 * Test method for
	 * {@link myob.service.PayslipInputValidator#validate(myob.domain.PayslipInputObject)}
	 * .
	 */
	@Test
	public void testValidateMissingLastName() {
		PayslipInputObject inputObject = Helper.populatePayslipObject();
		inputObject.setLastName("");
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("Last name");
	}

	/**
	 * Test: Negative annual salary.
	 * 
	 * Test method for
	 * {@link myob.service.PayslipInputValidator#validate(myob.domain.PayslipInputObject)}
	 * .
	 */
	@Test
	public void testValidateNegativeAnnualSalary() {
		PayslipInputObject inputObject = Helper.populatePayslipObject();
		inputObject.setAnnualSalary(-100);
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("salary");
	}

	/**
	 * Test: Super rate > 50%.
	 * 
	 * Test method for
	 * {@link myob.service.PayslipInputValidator#validate(myob.domain.PayslipInputObject)}
	 * .
	 */
	@Test
	public void testValidateSuperRateGreaterThan50() {
		PayslipInputObject inputObject = Helper.populatePayslipObject();

		// Super rate more than 50.
		inputObject.setSuperRate(51f);
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("Super rate");

		// Negative super rate.
		inputObject.setSuperRate(-5f);
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("Super rate");
	}

	/**
	 * Test: Super rate < 0%.
	 * 
	 * Test method for
	 * {@link myob.service.PayslipInputValidator#validate(myob.domain.PayslipInputObject)}
	 * .
	 */
	@Test
	public void testValidateSuperRateLessThan0() {
		PayslipInputObject inputObject = Helper.populatePayslipObject();

		// Super rate more than 50.
		inputObject.setSuperRate(-5f);
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("Super rate");

		// Negative super rate.
		inputObject.setSuperRate(-5f);
		assertThatThrownBy(() -> {
			validator.validate(inputObject);
		}).isInstanceOf(ValidationException.class).hasMessageContaining("Super rate");
	}
}
